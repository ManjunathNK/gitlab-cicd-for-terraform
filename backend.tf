terraform {
  backend "s3" {
    bucket = "manju-sample" # Replace with your actual S3 bucket name
    key    = "Gitlab/terraform.tfstate"
    region = "eu-central-1"
  }
}
